# Hello #

I think that the best choice for app's architecture is to build it using microservices. 

There are a few reasons why I think so:

* System must be scalable 
* There are to many features and entities
* Each feature has it's own responsibility and they don't have strong dependencies with each other
* Features will be likely redone in the future

We can't use Monolith  architecture, as it can't handle all the things in the list above.
The reason to avoid using DDD arch is because we don't have any really specific entities and processes.


According to the list of features, we can split our app into a list of microservices. Their dependency is shown on the diagram below

![Microservices scheme image](./images/microservices.png)



As for the protocol I0 think that classic HTTP with REST API architectural style will suit us perfectly.  
It is so, because :

* We don't have any actions that require rapid server responce, so don't need to use WebSocket
* We don't need advanced queu managing provided by AMQ protocol and RabbitMQ technology
* RESTfull servers are extremely good for using with microservices and APIs 